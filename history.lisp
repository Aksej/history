;;;; history.lisp

(in-package #:history)

(defstruct history
  value
  reason
  (parents nil :type list))

(defun build-bindings (g!vars)
  (mapcar (lambda (g!var)
            `(,g!var (make-history)))
          g!vars))

(defun build-setters (bindings g!vars g!curr)
  (when bindings
    (bind ((((var init &optional reason)
             &rest more-bindings)
            bindings)
           ((g!var &rest more-g!vars)
            g!vars))
      `(let ((,g!curr ,g!var))
         (declare (special ,g!curr))
         (declare (ignorable ,g!curr))
         (setf (history-value ,g!var)
               ,init
               (history-reason ,g!var)
               ,reason)
         ,@(when more-bindings
             `((labels
                   ((,var ()
                      ,g!var))
                 (declare (ignorable (function ,var)))
                 (symbol-macrolet
                     ((,var (progn
                              (pushnew ,g!var (history-parents ,g!curr))
                              (history-value ,g!var))))
                   ,(build-setters more-bindings more-g!vars g!curr)))))))))

(defun build-body (bindings g!vars body)
  `(labels
       (,@(mapcar (lambda (binding g!var)
                    `(,(first binding) () ,g!var))
            bindings g!vars))
     (declare (ignorable ,@(mapcar (lambda (binding)
                                     `(function ,(first binding)))
                                   bindings)))
     (symbol-macrolet
         (,@(mapcar (lambda (binding g!var)
                      `(,(first binding)
                        (history-value ,g!var)))
                    bindings g!vars))
       ,@body)))

(defmacro rlet* ((&rest bindings)
                 &body body)
  (bind ((g!vars (mapcar (lambda (binding)
                           (gensym (format nil "~A"
                                           (first binding))))
                         bindings))
         (g!curr (gensym "curr")))
    `(let (,@(build-bindings g!vars))
       ,(build-setters bindings g!vars g!curr)
       ,(build-body bindings g!vars body))))

(defgeneric explain (obj stream)
  (:method (obj stream)
    (format stream "~A"
            obj))
  (:method ((obj cons)
            stream)
    (format stream "~{~A~}"
            obj))
  (:method ((obj history)
            stream)
    (explain (history-reason obj)
             stream)))
