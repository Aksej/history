;;;; history.asd

(asdf:defsystem #:history
  :description "Describe history here"
  :author "Thomas Bartscher <thomas-bartscher@weltraumschlangen.de>"
  :license  "BSD-3"
  :version "0.0.1"
  :depends-on ("metabang-bind")
  :serial t
  :components
  ((:file "package")
   (:file "history")))
