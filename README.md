# history
### _Thomas Bartscher <thomas-bartscher@weltraumschlangen.de>_

A version of `let*` that keeps track of the dependencies between its
bindings. As a consequence, the bindings in `reasoned-let*` are not `setf`able.

## License

BSD-3

